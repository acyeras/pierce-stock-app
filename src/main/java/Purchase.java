import java.util.*;
public class Purchase {
    public static void main(String args[]) {
        Scanner scnr = new Scanner(System.in);
        System.out.println("Enter stock symbol: ");
        String symbol = scnr.next();
        System.out.println("Enter total shares: ");
        int shares = scnr.nextInt();
        System.out.println("Enter total cost: ");
        Double cost = scnr.nextDouble();
        Stock s = new Stock(symbol, shares, cost);

        System.out.println("Buying or selling? (Enter B/S, Q to quit) ");
        char T = Character.toUpperCase(scnr.next().charAt(0));
        while(T != 'Q') {
            if (T == 'B') {
                System.out.println("How many to buy? ");
                int buying = scnr.nextInt();
                s.buy(buying);
            }
            else if (T == 'S') {
                System.out.println("How many to sell? ");
                int selling = scnr.nextInt();
                s.sell(selling);
            }
            s.printShares();
            System.out.println();
            System.out.println("Buying or selling? (Enter B/S, Q to quit) ");
            T = Character.toUpperCase(scnr.next().charAt(0));
        }
        s.printShares();
    }
}