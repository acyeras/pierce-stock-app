public class Stock {
    private String symbol;
    private int totalShares;
    private Double totalCost, sharePrice;
    public Stock(String symbol, int totalShares, Double totalCost) {
        this.symbol = symbol;
        this.totalCost = totalCost;
        this.totalShares = totalShares;
        sharePrice = totalCost / totalShares;
    }

    public void buy(int buying) {
        totalShares += buying;
        totalCost = (sharePrice * totalShares);
    }

    public void sell(int buying) {
        totalShares -= buying;
        totalCost = (sharePrice * totalShares);
    }

    public void printShares() {
        System.out.println("Stock symbol: " + symbol);
        System.out.println("Stock total shares: " + totalShares);
        System.out.println("Stock total cost: " + totalCost);
    }
}